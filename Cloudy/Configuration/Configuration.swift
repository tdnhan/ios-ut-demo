//
//  Configuration.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

struct Defaults {
    static let Latitude: Double = 51.400592
    static let Longitude: Double = 4.760970
}

struct API {
    static let APIKey = "b2dbcec1f2f0cf92ddbeaef95dd05dce"
    static let BaseURL = URL(string: "https://api.darksky.net/forecast/")!
    
    static var AuthenticatedBaseURL: URL {
        return BaseURL.appendingPathComponent(APIKey)
    }
}
