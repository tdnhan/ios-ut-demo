//
//  CLLocation+Extensions.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocation {
    var string: String {
        let lat = String(format: "%.3f", coordinate.latitude)
        let lng = String(format: "%.3f", coordinate.longitude)
        return "\(lat), \(lng)"
    }
}
