//
//  Conversions.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

extension Double {
    func toCelcius() -> Double {
        return ((self - 32.0) / 1.8)
    }
    
    func toKPH() -> Double {
        return (self * 1.609344)
    }
}
