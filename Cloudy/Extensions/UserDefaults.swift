//
//  UserDefaults.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation


enum TimeNotation: Int {
    case twelveHour, twentyFourHour
    
    var timeFormat: String {
        switch self {
        case .twelveHour: return "hh:mm a"
        case .twentyFourHour: return "HH: mm"
        }
    }
}

enum UnitsNotation: Int {
    case imperial, metric
}

enum TemperatureNotation: Int {
    case fahrenheit, celsius
}

struct UserDefaultsKeys {
    static let locations = "locations"
    static let timeNotation = "timeNotation"
    static let unitsNotation = "unitsNotation"
    static let temperatureNotation = "temperatureNotation"
}

extension UserDefaults {
    // MARK: - Time Notation
    static var timeNotation: TimeNotation {
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: UserDefaultsKeys.timeNotation)
        }
        get {
            let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.timeNotation)
            return TimeNotation(rawValue: storedValue) ?? TimeNotation.twelveHour
        }
    }
    
    // MARK: - Units Notation
    static var unitsNotation: UnitsNotation {
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: UserDefaultsKeys.unitsNotation)
        }
        get {
            let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.unitsNotation)
            return UnitsNotation(rawValue: storedValue) ?? UnitsNotation.imperial
        }
    }
    
    // MARK: - Temperature Notation
    static var temperatureNotation: TemperatureNotation {
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: UserDefaultsKeys.temperatureNotation)
        }
        get {
            let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.temperatureNotation)
            return TemperatureNotation(rawValue: storedValue) ?? TemperatureNotation.fahrenheit
        }
    }
}

extension UserDefaults {
    // MARK: - Locations
    static func loadLocations() -> [Location] {
        guard let dictionaries = UserDefaults.standard.array(forKey: UserDefaultsKeys.locations) as? [[String: Any]] else {
            return []
        }
        return dictionaries.compactMap({ Location(dictionary: $0) })
    }
    
    static func addLocation(_ location: Location) {
        var locations = loadLocations()
        locations.append(location)
        saveLocations(locations)
    }
    
    static func removeLocation(_ location: Location) {
        var locations = loadLocations()
        guard let index = locations.firstIndex(of: location) else {
            return
        }
        locations.remove(at: index)
        
        saveLocations(locations)
    }
    
    private static func saveLocations(_ locations: [Location]) {
        let dicts = locations.map({ $0.dictionary })
        UserDefaults.standard.set(dicts, forKey: UserDefaultsKeys.locations)
    }
}
