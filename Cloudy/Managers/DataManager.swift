//
//  DataManager.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

enum DataManagerError: Error {
    case unknow
    case failedRequest
    case invalidResponse
}

final class DataManager {
    typealias WeatherDataCompletion = (WeatherData?, DataManagerError?) -> ()
    
    // MARK: - Properties
    private let baseURL: URL
    
    // MARK: - Initialization
    init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    func weatherDataForLocation(latitude: Double, longitude: Double, completion: @escaping WeatherDataCompletion) {
        let URL = baseURL.appendingPathComponent("\(latitude),\(longitude)")
        
        URLSession.shared.dataTask(with: URL) { (data, response, error) in
            DispatchQueue.main.async { [weak self] in
                self?.didFetchWeatherData(data: data, response: response, error: error, completion: completion)
            }
        }.resume()
    }
    
    private func didFetchWeatherData(data: Data?, response: URLResponse?, error: Error?, completion: WeatherDataCompletion) {
        guard error == nil else {
            completion(nil, .failedRequest)
            return
        }
        
        guard let data = data, let response = response as? HTTPURLResponse else {
            completion(nil, .unknow)
            return
        }
        
        if response.statusCode == 200 {
            do {
                let weatherData = try JSONDecoder().decode(WeatherData.self, from: data)
                completion(weatherData, nil)
            } catch {
                // Invoke Completion Handler
                completion(nil, .invalidResponse)
            }
        } else {
            completion(nil, .failedRequest)
        }
    }
}
