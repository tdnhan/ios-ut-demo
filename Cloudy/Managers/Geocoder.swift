//
//  Geocoder.swift
//  Cloudy
//
//  Created by tdnhan on 2/21/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import CoreLocation

class Geocoder: LocationService {
    // MARK: - Properties
    private lazy var geocoder = CLGeocoder()
    
    // MARK: - Location Service Protocol
    func geocode(addressString: String?, completionHandler: @escaping LocationServiceComletionHandler) {
        guard let addressString = addressString else {
            completionHandler([], nil)
            return
        }
        
        // Geocode Address String
        geocoder.geocodeAddressString(addressString) { (placemarks, error) in
            var locations: [Location] = []
            
            if let error = error {
                print("Unable to Forward Geocode Address: \(error)")
            } else if let matches = placemarks {
                // Update Locations
                locations = matches.compactMap({ (match) -> Location? in
                    guard let name = match.name,
                        let location = match.location else { return nil }
                    return Location(name: name, latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                })
                
                completionHandler(locations, nil)
            }
        }
    }
}
