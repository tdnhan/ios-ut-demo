//
//  LocationService.swift
//  Cloudy
//
//  Created by tdnhan on 2/21/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

protocol LocationService {
    typealias LocationServiceComletionHandler = ([Location], Error?) -> Void
    
    func geocode(addressString: String?, completionHandler: @escaping LocationServiceComletionHandler)
}
