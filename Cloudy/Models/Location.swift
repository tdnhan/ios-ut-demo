//
//  Location.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation
import CoreLocation

struct Location {
    private enum Keys {
        static let name = "name"
        static let latitude = "latitude"
        static let longitude = "longitude"
    }
    
    // MARK: - Properties
    let name: String
    let latitude: Double
    let longitude: Double
    
    var location: CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    var dictionary: [String: Any] {
        return [Keys.name: name,
                Keys.latitude: latitude,
                Keys.longitude: longitude]
    }
}

extension Location: Codable {
    // MARK: - Initialization
    init?(dictionary: [String: Any]) {
        guard let name = dictionary[Keys.name] as? String,
            let lat = dictionary[Keys.latitude] as? Double,
            let lng = dictionary[Keys.longitude] as? Double else { return nil }
        self.name = name
        self.latitude = lat
        self.longitude = lng
    }
}

extension Location: Equatable {
    static func ==(lhs: Location, rhs: Location) -> Bool {
        return lhs.name == rhs.name &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude
    }
}
