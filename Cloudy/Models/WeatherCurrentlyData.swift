//
//  WeatherCurrentlyData.swift
//  Cloudy
//
//  Created by tdnhan on 2/21/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

struct WeatherCurrentlyData: Codable {
    let time: Date
    let summary: String
    let icon: String
    let windSpeed: Double
    let temperature: Double
}
