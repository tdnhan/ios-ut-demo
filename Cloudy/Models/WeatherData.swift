//
//  WeatherData.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

struct WeatherData: Codable {
    let currently: WeatherCurrentlyData
    let latitude: Double
    let longitude: Double
    
    let daily: WeatherDailyDayData
}
