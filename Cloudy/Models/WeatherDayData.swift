//
//  WeatherDayData.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

struct WeatherDayData: Codable {
    let time: Date
    let summary: String
    let icon: String
    let windSpeed: Double
    let temperatureMin: Double
    let temperatureMax: Double
}
