//
//  AddLocationViewViewModel.swift
//  Cloudy
//
//  Created by tdnhan on 2/21/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import CoreLocation

class AddLocationViewViewModel {
    private var locations: [Location] = []
    private let locationService: LocationService
    
    var onLoadedLocations: (([Location]) -> Void)?
    
    init(query: String?, locationService: LocationService) {
        self.locationService = locationService
        geocode(addressString: query)
    }
    
    var numberOfRows: Int {
        return locations.count
    }
    
    func location(for index: Int) -> Location {
        return locations[index]
    }
    
    func viewModel(for index: Int) -> LocationsViewViewModel {
        let location = locations[index]
        return LocationsViewViewModel(location: location.location, locationAsString: location.name)
    }
    
    func geocode(addressString: String?) {
        guard let addressString = addressString, !addressString.isEmpty else {
            locations = []
            onLoadedLocations?(locations)
            return
        }
        
        locationService.geocode(addressString: addressString) { [weak self] (locations, error) in
            self?.locations = locations
            
            if let error = error {
                print("Unable to Forward Geocode Address: \(error)")
            }
            DispatchQueue.main.async {
                self?.onLoadedLocations?(locations)
            }
        }
    }
}
