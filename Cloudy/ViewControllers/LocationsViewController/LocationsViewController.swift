//
//  LocationsViewController.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationsViewControllerDelegate: class {
    func controller(_ controller: LocationsViewController, didSelectLocation location: CLLocation)
}

class LocationsViewController: UIViewController {
    private let segueAddLocationView = "SegueAddLocationView"
    
    @IBOutlet weak var tableView: UITableView!
    
    var currentLocation: CLLocation?
    
    var favorites = UserDefaults.loadLocations()
    
    private var hasFavorites: Bool {
        return favorites.count > 0
    }
    
    weak var delegate: LocationsViewControllerDelegate?

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Locations"
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch identifier {
        case segueAddLocationView:
            if let addLocationViewController = segue.destination as? AddLocationViewController {
                addLocationViewController.delegate = self
            } else {
                fatalError("Unexpected Destination View Controller")
            }
        default:
            break
        }
    }
    
    @IBAction func unwindToLocationsViewController(segue: UIStoryboardSegue) {
    }
}

extension LocationsViewController: UITableViewDataSource {
    
    private enum Section: Int {
        case current
        case favorite
        
        var title: String {
            switch self {
            case .current: return "Current Location"
            case .favorite: return "Favorite Locations"
            }
        }
        
        var numberOfRows: Int {
            switch self {
            case .current: return 1
            case .favorite: return 0
            }
        }
        
        static var count: Int {
            return Section.favorite.rawValue + 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section(rawValue: section) else {
            fatalError("Unexpected Section")
        }
        
        switch section {
        case .current: return 1
        case .favorite: return max(favorites.count, 1)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let section = Section(rawValue: section) else {
            fatalError("Unexpected Section")
        }
        return section.title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: indexPath.section) else {
            fatalError("Unexpected Section")
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LocationTableViewCell.reuseIdentifier, for: indexPath) as? LocationTableViewCell else {
            fatalError("Unexpected Table View Cell")
        }
        
        var viewModel: LocationRepresentable?
        switch section {
        case .current:
            if let currentLocation = currentLocation {
                viewModel = LocationsViewViewModel(location: currentLocation)
            } else {
                cell.mainLabel.text = "Current Location Unknown"
            }
        case .favorite:
            if favorites.count > 0 {
                // Fetch favorite
                let favorite = favorites[indexPath.row]
                
                viewModel = LocationsViewViewModel(location: favorite.location, locationAsString: favorite.name)
            } else {
                cell.mainLabel.text = "No Favorites Found"
            }
        }
        
        if let viewModel = viewModel {
            cell.configure(withViewModel: viewModel)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let section = Section(rawValue: indexPath.section) else {
            fatalError("Unexpected Section")
        }
        
        switch section {
        case .current: return false
        case .favorite: return hasFavorites
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let location = favorites[indexPath.row]
        
        favorites.remove(at: indexPath.row)
        
        UserDefaults.removeLocation(location)
        
        tableView.deleteRows(at: [indexPath], with: .fade)
    }
}

extension LocationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = Section(rawValue: indexPath.section) else {
            fatalError("Unexpected Section")
        }
        
        var locaton: CLLocation?
        switch section {
        case .current:
            if let curLocation = currentLocation {
                locaton = curLocation
            }
        case .favorite:
            if hasFavorites {
                locaton = favorites[indexPath.row].location
            }
        }
        
        if let newLocation = locaton {
            // Notify Delegate
            delegate?.controller(self, didSelectLocation: newLocation)
            
            // Dismiss View Controller
            dismiss(animated: true, completion: nil)
        }
    }
}

extension LocationsViewController: AddLocationViewControllerDelegate {
    func controller(_ controller: AddLocationViewController, didAddLocation location: Location) {
        UserDefaults.addLocation(location)
        favorites.append(location)
        tableView.reloadData()
    }
}
