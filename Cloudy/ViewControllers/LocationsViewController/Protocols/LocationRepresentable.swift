//
//  LocationRepresentable.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

protocol LocationRepresentable {
    var text: String { get }
}
