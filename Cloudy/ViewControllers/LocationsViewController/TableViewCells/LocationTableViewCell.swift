//
//  LocationTableViewCell.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {
    
    // MARK: - Type Properties
    static let reuseIdentifier = "LocationCell"
    
    // MARK: - Properties
    @IBOutlet var mainLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(withViewModel viewModel: LocationRepresentable) {
        mainLabel.text = viewModel.text
    }
}
