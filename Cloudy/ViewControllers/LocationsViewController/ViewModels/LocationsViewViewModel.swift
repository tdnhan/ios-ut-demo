//
//  LocationsViewViewModel.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationsViewViewModel {
    // MARK: - Properties
    let location: CLLocation?
    let locationAsString: String?
    
    // MARK: - Initialization
    init(location: CLLocation? = nil, locationAsString: String? = nil) {
        self.location = location
        self.locationAsString = locationAsString
    }
}

extension LocationsViewViewModel: LocationRepresentable {
    var text: String {
        if let locationStr = locationAsString {
            return locationStr
        } else if let location = location {
            return location.string
        }
        
        return "Unknown Location"
    }
}

