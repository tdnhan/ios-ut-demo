//
//  RootViewController.swift
//  Cloudy
//
//  Created by tdnhan on 2/18/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit
import CoreLocation

class RootViewController: UIViewController {
    
    // MARK: - Constants
    private let segueDayView = "SegueDayView"
    private let segueWeekView = "SegueWeekView"
    private let segueSettingsView = "SegueSettingsView"
    private let segueLocationsView = "SegueLocationsView"
    
    // MARK: - Properties
    private var dayViewController: DayViewController!
    private var weekViewController: WeekViewController!
    
    private var currentLocation: CLLocation? {
        didSet {
            fetchWeatherData()
        }
    }
    
    private lazy var dataManager = {
        return DataManager(baseURL: API.AuthenticatedBaseURL)
    }()
    
    private lazy var locationManager: CLLocationManager = {
       let locationManager = CLLocationManager()
        
        locationManager.distanceFilter = 1000.0
        locationManager.desiredAccuracy = 1000.0
        
        return locationManager
    }()

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = []
        setupNotificationHandling()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case segueDayView:
            guard let destination = segue.destination as? DayViewController else {
                fatalError("Unexpected Destination View Controller")
            }
            
            destination.delegate = self
            self.dayViewController = destination
        case segueWeekView:
            guard let destination = segue.destination as? WeekViewController else {
                fatalError("Unexpected Destination View Controller")
            }
            
            destination.delegate = self
            self.weekViewController = destination
        case segueSettingsView:
            guard let navigationController = segue.destination as? UINavigationController,
                let destination = navigationController.topViewController as? SettingsViewController else {
                fatalError("Unexpected Destination View Controller")
            }
            
            destination.delegate = self
        case segueLocationsView:
            guard let navigationController = segue.destination as? UINavigationController,
                let destination = navigationController.topViewController as? LocationsViewController else {
                fatalError("Unexpected Destination View Controller")
            }
            
            destination.delegate = self
            destination.currentLocation = currentLocation
        default: break
        }
    }
    
    @IBAction func unwindToRootViewController(segue: UIStoryboardSegue) {
    }
    
    // MARK: - Notification handling
    @objc func applicationDidBecomeActive(notification: Notification) {
        requestLocation()
    }
}

private extension RootViewController {
    func setupNotificationHandling() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(RootViewController.applicationDidBecomeActive(notification:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func requestLocation() {
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func fetchWeatherData() {
        guard let location = currentLocation else { return }
        
        let lat = location.coordinate.latitude
        let lng = location.coordinate.longitude
        
        print("\(lat), \(lng)")
        
        dataManager.weatherDataForLocation(latitude: lat, longitude: lng) { [weak self] (data, error) in
            if let error = error {
                print(error)
            } else if let data = data {
                self?.dayViewController.viewModel = DayViewViewModel(weatherData: data)
                self?.weekViewController.viewModel = WeekViewViewModel(weatherDailyData: data.daily)
            }
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension RootViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        } else {
            currentLocation = CLLocation(latitude: Defaults.Latitude, longitude: Defaults.Longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
            manager.delegate = nil
            manager.stopUpdatingLocation()
        } else {
            currentLocation = CLLocation(latitude: Defaults.Latitude, longitude: Defaults.Longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        if currentLocation == nil {
            currentLocation = CLLocation(latitude: Defaults.Latitude, longitude: Defaults.Longitude)
        }
    }
}

// MARK: - DayViewControllerDelegate
extension RootViewController: DayViewControllerDelegate {
    func controllerDidTapSettingsButton(controller: DayViewController) {
        performSegue(withIdentifier: segueSettingsView, sender: self)
    }
    
    func controllerDidTapLocationButton(controller: DayViewController) {
        performSegue(withIdentifier: segueLocationsView, sender: self)
    }
}

// MARK: - WeekViewControllerDelegate {
extension RootViewController: WeekViewControllerDelegate {
    func controllerDidRefresh(controller: WeekViewController) {
        fetchWeatherData()
    }
}

// MARK: - SettingsViewControllerDelegate
extension RootViewController: SettingsViewControllerDelegate {
    func controllerDidChangeTimeNotation(controller: SettingsViewController) {
        dayViewController.reloadData()
        weekViewController.reloadData()
    }
    
    func controllerDidChangeUnitsNotation(controller: SettingsViewController) {
        dayViewController.reloadData()
        weekViewController.reloadData()
    }
    
    func controllerDidChangeTemperatureNotation(controller: SettingsViewController) {
        dayViewController.reloadData()
        weekViewController.reloadData()
    }
}

// MARK: - LocationsViewControllerDelegate
extension RootViewController: LocationsViewControllerDelegate {
    func controller(_ controller: LocationsViewController, didSelectLocation location: CLLocation) {
        currentLocation = location
    }
}
