//
//  SettingsRepresentable.swift
//  Cloudy
//
//  Created by tdnhan on 2/20/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

protocol SettingsRepresentable {
    var text: String { get }
    var accessoryType: UITableViewCell.AccessoryType { get }
}
