//
//  SettingsViewController.swift
//  Cloudy
//
//  Created by tdnhan on 2/20/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

protocol SettingsViewControllerDelegate: class {
    func controllerDidChangeTimeNotation(controller: SettingsViewController)
    func controllerDidChangeUnitsNotation(controller: SettingsViewController)
    func controllerDidChangeTemperatureNotation(controller: SettingsViewController)
}

class SettingsViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet var tableView: UITableView!
    
    weak var delegate: SettingsViewControllerDelegate?

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        setupView()
    }
}

private extension SettingsViewController {
    func setupView() {
        setupTableView()
    }
    
    func setupTableView() {
        tableView.separatorInset = .zero
    }
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {
    private enum Section: Int {
        case time, units, temperature
        
        var numberOfRows: Int {
            return 2
        }
        
        static var count: Int {
            return Section.temperature.rawValue + 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Section(rawValue: section)?.numberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: indexPath.section) else {
            fatalError("Unexpected Section")
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.reuseIdentifier, for: indexPath) as? SettingsTableViewCell else {
            fatalError("Unexpected Table View Cell")
        }
        
        var viewModel: SettingsRepresentable?
        
        switch section {
        case .time:
            guard let timeNotation = TimeNotation(rawValue: indexPath.row) else {
                fatalError("Unexpected Index Path")
            }
            viewModel = SettingsViewTimeViewModel(timeNotation: timeNotation)
        case .units:
            guard let unitsNotation = UnitsNotation(rawValue: indexPath.row) else {
                fatalError("Unexpected Index Path")
            }
            viewModel = SettingsViewUnitsViewModel(unitsNotation: unitsNotation)
        case .temperature:
            guard let temperatureNotation = TemperatureNotation(rawValue: indexPath.row) else {
                fatalError("Unexpected Index Path")
            }
            viewModel = SettingsViewTemperatureViewModel(temperatureNotation: temperatureNotation)
        }
        
        if let viewModel = viewModel {
            cell.configure(withViewModel: viewModel)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let section = Section(rawValue: indexPath.section) else {
            fatalError("Unexpected Section")
        }
        
        switch section {
        case .time:
            let timeNotation = UserDefaults.timeNotation
            guard indexPath.row != timeNotation.rawValue else { return }

            if let newTimeNotation = TimeNotation(rawValue: indexPath.row) {
                // Update User Defaults
                UserDefaults.timeNotation = newTimeNotation

                // Notify Delegate
                delegate?.controllerDidChangeTimeNotation(controller: self)
            }
        case .units:
            let unitsNotation = UserDefaults.unitsNotation
            guard indexPath.row != unitsNotation.rawValue else { return }

            if let newUnitsNotation = UnitsNotation(rawValue: indexPath.row) {
                // Update User Defaults
                UserDefaults.unitsNotation = newUnitsNotation

                // Notify Delegate
                delegate?.controllerDidChangeUnitsNotation(controller: self)
            }
        case .temperature:
            let temperatureNotation = UserDefaults.temperatureNotation
            guard indexPath.row != temperatureNotation.rawValue else { return }

            if let newTemperatureNotation = TemperatureNotation(rawValue: indexPath.row) {
                // Update User Defaults
                UserDefaults.temperatureNotation = newTemperatureNotation

                // Notify Delegate
                delegate?.controllerDidChangeTemperatureNotation(controller: self)
            }
        }
        
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
    }
}
