//
//  SettingsTableViewCell.swift
//  Cloudy
//
//  Created by tdnhan on 2/20/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    // MARK: - Type Properties
    static let reuseIdentifier = "SettingsCell"

    // MARK: - Properties
    @IBOutlet weak var mainLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Configuration
    func configure(withViewModel viewModel: SettingsRepresentable) {
        mainLabel.text = viewModel.text
        accessoryType = viewModel.accessoryType
    }
}
