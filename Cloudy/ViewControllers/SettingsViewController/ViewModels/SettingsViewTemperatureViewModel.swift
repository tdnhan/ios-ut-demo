//
//  SettingsViewTemperatureViewModel.swift
//  Cloudy
//
//  Created by tdnhan on 2/20/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

struct SettingsViewTemperatureViewModel {
    // MARK: - Properties
    let temperatureNotation: TemperatureNotation
    
    // MARK: - Public Interface
    var text: String {
        switch temperatureNotation {
        case .fahrenheit: return "Fahrenheit"
        case .celsius: return "Celsius"
        }
    }
        
    var accessoryType: UITableViewCell.AccessoryType {
        return UserDefaults.temperatureNotation == temperatureNotation ? .checkmark : .none
    }
}

extension SettingsViewTemperatureViewModel: SettingsRepresentable { }
