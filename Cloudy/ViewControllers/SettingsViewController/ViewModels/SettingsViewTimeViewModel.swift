//
//  SettingsViewTimeViewModel.swift
//  Cloudy
//
//  Created by tdnhan on 2/20/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

struct SettingsViewTimeViewModel {
    // MARK: - Properties
    let timeNotation: TimeNotation
    
    var text: String {
        switch timeNotation {
        case .twelveHour:
            return "12 Hour"
        case .twentyFourHour:
            return "24 Hour"
        }
    }
    
    var accessoryType: UITableViewCell.AccessoryType {
        return UserDefaults.timeNotation == timeNotation ? .checkmark : .none
    }
}

extension SettingsViewTimeViewModel: SettingsRepresentable { }
