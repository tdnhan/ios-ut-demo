//
//  SettingsViewUnitsViewModel.swift
//  Cloudy
//
//  Created by tdnhan on 2/20/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

struct SettingsViewUnitsViewModel {
    // MARK: - Properties
    let unitsNotation: UnitsNotation
    
    // MARK: - Public Interface
    var text: String {
        switch unitsNotation {
        case .imperial: return "Imperial"
        case .metric: return "Metric"
        }
    }
    
    var accessoryType: UITableViewCell.AccessoryType {
        return UserDefaults.unitsNotation == unitsNotation ? .checkmark : .none
    }
}

extension SettingsViewUnitsViewModel: SettingsRepresentable { }
