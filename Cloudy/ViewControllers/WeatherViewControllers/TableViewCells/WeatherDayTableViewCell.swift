//
//  WeatherDayTableViewCell.swift
//  Cloudy
//
//  Created by tdnhan on 2/20/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

class WeatherDayTableViewCell: UITableViewCell {
    
    // MARK: - Type Properties
    static let reuseIdentifier = "WeatherDayCell"
    
    // MARK: - Properties
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Configure Cell
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Configuration
    func configure(withViewModel viewModel: WeatherDayRepresentable) {
        dayLabel.text = viewModel.day
        dateLabel.text = viewModel.date
        iconImageView.image = viewModel.image
        windSpeedLabel.text = viewModel.windSpeed
        temperatureLabel.text = viewModel.temperature
    }
}
