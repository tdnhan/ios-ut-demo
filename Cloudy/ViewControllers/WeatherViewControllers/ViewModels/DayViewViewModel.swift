//
//  DayViewViewModel.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

struct DayViewViewModel {
    let weatherData: WeatherData
    
    private let dateFormatter = DateFormatter()
    private let timeFormatter = DateFormatter()
    
    var date: String {
        dateFormatter.dateFormat = "EEE, MMMM d"
        return dateFormatter.string(from: weatherData.currently.time)
    }
    
    var time: String {
        timeFormatter.dateFormat = UserDefaults.timeNotation.timeFormat
        return timeFormatter.string(from: weatherData.currently.time)
    }
    
    var summary: String {
        return weatherData.currently.summary
    }
    
    var temperature: String {
        let temp = weatherData.currently.temperature
        switch UserDefaults.temperatureNotation {
        case .fahrenheit: return String(format: "%.1f °F", temp)
        case .celsius: return String(format: "%.1f °C", temp.toCelcius())
        }
    }
    
    var windSpeed: String {
        let windSpeed = weatherData.currently.windSpeed
        
        switch UserDefaults.unitsNotation {
        case .imperial: return String(format: "%.f MPH", windSpeed)
        case .metric: return String(format: "%.f KPH", windSpeed.toKPH())
        }
    }
    
    var image: UIImage? {
        return UIImage.imageForIcon(withName: weatherData.currently.icon)
    }
    
}
