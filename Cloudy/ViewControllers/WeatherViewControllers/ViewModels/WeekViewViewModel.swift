//
//  WeekViewViewModel.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

struct WeekViewViewModel {
    let weatherDailyData: WeatherDailyDayData
    
    var numberOfSections: Int {
        return 1
    }
    
    var numberOfDays: Int {
        return weatherDailyData.data.count
    }
    
    func viewModel(for index: Int) -> WeatherDayViewViewModel {
        return WeatherDayViewViewModel(weatherDayData: weatherDailyData.data[index])
    }
}
