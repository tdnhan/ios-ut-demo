//
//  WeatherViewController.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    // MARK: - Properties
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var weatherDataContainer: UIView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func reloadData() { }
    
    // MARK: - View Methods
    private func setupView() {
        messageLabel.isHidden = true
        weatherDataContainer.isHidden = true
        activityIndicatorView.startAnimating()
        activityIndicatorView.hidesWhenStopped = true
    }

    func imageForIcon(withName name: String) -> UIImage? {
        switch name {
        case "clear-day":
            return UIImage(named: "clear-day")
        case "clear-night":
            return UIImage(named: "clear-night")
        case "rain":
            return UIImage(named: "rain")
        case "snow":
            return UIImage(named: "snow")
        case "sleet":
            return UIImage(named: "sleet")
        case "wind", "cloudy", "partly-cloudy-day", "partly-cloudy-night":
            return UIImage(named: "cloudy")
        default:
            return UIImage(named: "clear-day")
        }
    }
}
