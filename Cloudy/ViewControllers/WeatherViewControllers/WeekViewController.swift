//
//  WeekViewController.swift
//  Cloudy
//
//  Created by tdnhan on 2/19/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import UIKit

protocol WeekViewControllerDelegate: class {
    func controllerDidRefresh(controller: WeekViewController)
}

class WeekViewController: WeatherViewController {
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: WeekViewControllerDelegate?
    
    var viewModel: WeekViewViewModel? {
        didSet {
            updateView()
        }
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func reloadData() {
        updateView()
    }
    
    @objc func didRefresh(sender: UIRefreshControl) {
        delegate?.controllerDidRefresh(controller: self)
    }
}

private extension WeekViewController {
    func setupView() {
        setupTableView()
        setupRefreshControl()
    }
    
    func updateView() {
        activityIndicatorView.stopAnimating()
        tableView.refreshControl?.endRefreshing()
        
        if viewModel != nil {
            updateWeatherDataContainer()
        } else {
            messageLabel.isHidden = false
            messageLabel.text = "Cloudy was unable to fetch weather data."
        }
    }
    
    func setupTableView() {
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    func setupRefreshControl() {
        // Initialize Refresh Control
        let refreshControl = UIRefreshControl()
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(WeekViewController.didRefresh(sender:)), for: .valueChanged)
        
        // Update Table View
        tableView.refreshControl = refreshControl
    }
    
    func updateWeatherDataContainer() {
        weatherDataContainer.isHidden = false
        
        tableView.reloadData()
    }
}

extension WeekViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfDays ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherDayTableViewCell.reuseIdentifier, for: indexPath) as? WeatherDayTableViewCell else {
            fatalError("Unexpected Table View Cell")
        }
        if let viewModel = viewModel?.viewModel(for: indexPath.row) {
            cell.configure(withViewModel: viewModel)
        }
        return cell
    }
}
