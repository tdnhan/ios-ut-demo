//
//  Codable+Extensions.swift
//  CloudyTests
//
//  Created by tdnhan on 2/24/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

extension Encodable {
    var jsonDict: [String: Any]? {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(self)
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
        } catch {
            print("Error converting to dict: \(error)")
            return nil
        }
    }
}
