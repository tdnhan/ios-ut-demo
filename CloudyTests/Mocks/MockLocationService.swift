//
//  LocationServiceMock.swift
//  CloudyTests
//
//  Created by tdnhan on 2/21/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation
@testable import Cloudy

class MockLocationService: LocationService {
    var shouldGeoCodeError: Bool = false
    
    func geocode(addressString: String?, completionHandler: @escaping LocationServiceComletionHandler) {
        guard !shouldGeoCodeError else {
            completionHandler([], DataManagerError.unknow)
            return
        }
        if let addressString = addressString, !addressString.isEmpty {
            // Create Location
            let location = Location(name: "Da Nang", latitude: 16.050917, longitude: 108.209593)
            
            // Invoke Completion Handler
            completionHandler([location], nil)
        } else {
            completionHandler([], nil)
        }
    }
}
