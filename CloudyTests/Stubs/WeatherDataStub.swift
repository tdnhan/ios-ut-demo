//
//  WeatherDataStub.swift
//  CloudyTests
//
//  Created by tdnhan on 2/24/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation

@testable import Cloudy

extension WeatherData {
    static func createStub() -> WeatherData {
        var dict: [String: Any] = ["latitude": 51.400592,
                                   "longitude": 4.76097]
        if let currently = WeatherCurrentlyData.createStub().jsonDict {
            dict["currently"] = currently
        }
        if let daily = WeatherDailyDayData.createStub().jsonDict {
            dict["daily"] = daily
        }
        
        let decoder = JSONDecoder()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let obj = try decoder.decode(WeatherData.self, from: jsonData)
            return obj
        } catch {
            fatalError("Cannot parse object")
        }
    }
}

extension WeatherCurrentlyData {
    static func createStub() -> WeatherCurrentlyData {
        let dict: [String: Any] = ["time": 1582514415,
                                   "summary": "Overcast",
                                   "icon": "Cloudy",
                                   "windSpeed": 5.61,
                                   "temperature": 38.88]
        let decoder = JSONDecoder()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let obj = try decoder.decode(WeatherCurrentlyData.self, from: jsonData)
            return obj
        } catch {
            fatalError("Cannot parse object")
        }
    }
}

extension WeatherDailyDayData {
    static func createStub() -> WeatherDailyDayData {
        let dict: [String: Any] = ["summary": "Light rain throughout the week.",
                                   "icon": "rain",
                                   "data": []]
        let decoder = JSONDecoder()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let obj = try decoder.decode(WeatherDailyDayData.self, from: jsonData)
            return obj
        } catch {
            fatalError("Cannot parse object")
        }
    }
}
