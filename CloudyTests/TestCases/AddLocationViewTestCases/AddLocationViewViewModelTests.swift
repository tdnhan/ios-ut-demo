//
//  AddLocationViewViewModelTests.swift
//  CloudyTests
//
//  Created by tdnhan on 2/21/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import XCTest
@testable import Cloudy

class AddLocationViewViewModelTests: XCTestCase {
    
    var locationService: MockLocationService!
    var viewModel: AddLocationViewViewModel!

    override func setUp() {
        super.setUp()
        
        // Initialize Location Service
        locationService = MockLocationService()
        
        // Initialize View Model
        viewModel = AddLocationViewViewModel(query: "xxx", locationService: locationService)
    }

    override func tearDown() {
        super.tearDown()
        UserDefaults.standard.set([], forKey: UserDefaultsKeys.locations)
    }
    
    func testLocations_HasLocations() {
        viewModel.geocode(addressString: "xxx")
        
        let location = viewModel.location(for: 0)
        XCTAssertNotNil(location)
        XCTAssertEqual(location.name, "Da Nang")
        XCTAssertEqual(viewModel.viewModel(for: 0).text, "Da Nang")
        XCTAssert(viewModel.numberOfRows > 0)
    }
    
    func testLocations_NoLocations() {
        viewModel.geocode(addressString: "")
        
        XCTAssertEqual(viewModel.numberOfRows, 0)
    }
    
    func testLocations_geocodeWithError() {
        locationService.shouldGeoCodeError = true
        viewModel.geocode(addressString: "xxx")
        
        XCTAssert(viewModel.numberOfRows == 0)
    }
}
