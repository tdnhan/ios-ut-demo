//
//  DayViewViewModelTests.swift
//  CloudyTests
//
//  Created by tdnhan on 2/24/20.
//  Copyright © 2020 AxonActive. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Cloudy

class DayViewViewModelSpec: QuickSpec {
    override func spec() {
        let viewModel = DayViewViewModel(weatherData: WeatherData.createStub())
        
        describe("DayViewViewModek") {
            beforeEach {
                UserDefaults.standard.set(TemperatureNotation.fahrenheit.rawValue, forKey: UserDefaultsKeys.temperatureNotation)
                UserDefaults.standard.set(UnitsNotation.imperial.rawValue, forKey: UserDefaultsKeys.unitsNotation)
            }
            
            context("is created") {
                it("has right informations") {
                    expect(viewModel.date) == "Fri, February 24"
                    expect(viewModel.time) != "10:10"
                    expect(viewModel.summary) == "Overcast"
                    expect(viewModel.image).toNot(beNil())
                }
                
                it("has right temperature format following fehrenheit settings") {
                    UserDefaults.standard.set(TemperatureNotation.fahrenheit.rawValue, forKey: UserDefaultsKeys.temperatureNotation)
                    expect(viewModel.temperature).to(equal("38.9 °F"))
                }
                
                it("has right temperature format following celsius settings") {
                    UserDefaults.standard.set(TemperatureNotation.celsius.rawValue, forKey: UserDefaultsKeys.temperatureNotation)
                    expect(viewModel.temperature).to(equal("3.8 °C"))
                }
                
                it("has right windSpped format following imperial settings") {
                    UserDefaults.standard.set(UnitsNotation.imperial.rawValue, forKey: UserDefaultsKeys.unitsNotation)
                    expect(viewModel.windSpeed).to(equal("6 MPH"))
                }
                
                it("has right windSpped format following metric settings") {
                    UserDefaults.standard.set(UnitsNotation.metric.rawValue, forKey: UserDefaultsKeys.unitsNotation)
                    expect(viewModel.windSpeed).to(equal("9 KPH"))
                }
            }
        }
    }
}
